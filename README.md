`metrics-build` is the reproducible build system for Metrics artifacts which is
based on [rbm](https://gitlab.torproject.org/tpo/applications/rbm).

# Installing build dependencies

To build Metrics artifacts, you need a recent Linux distribution with support
for user_namespaces(7) (such as Debian Bullseye, Ubuntu 18.04, Fedora 30,
etc ...). You will need to install the `uidmap` package, providing the
`newuidmap` and `newgidmap` commands.

The sources of most components are downloaded using `git`, which needs to
be installed.

There are some additional packaged needed as well. If you are running Debian or
Ubuntu, you can install them with:

```
apt-get install libyaml-libyaml-perl libtemplate-perl libdatetime-perl \
                libio-captureoutput-perl libpath-tiny-perl \
                libstring-shellquote-perl libfile-copy-recursive-perl \
                libsort-versions-perl libdata-uuid-perl git uidmap
