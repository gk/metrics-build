rbm=./rbm/rbm

all: release-metrics-lib

release-collector: submodule-update
	$(rbm) build collector --target release-build

dev-collector: submodule-update
	$(rbm) build collector --target dev-build

release-exonerator: submodule-update
	$(rbm) build exonerator --target release-build

dev-exonerator: submodule-update
	$(rbm) build exonerator --target dev-build

release-metrics-lib: submodule-update
	$(rbm) build metrics-lib --target release-build

dev-metrics-lib: submodule-update
	$(rbm) build metrics-lib --target dev-build

release-metrics-web: submodule-update
	$(rbm) build metrics-web --target release-build

dev-metrics-web: submodule-update
	$(rbm) build metrics-web --target dev-build

release-onionoo: submodule-update
	$(rbm) build onionoo --target release-build

dev-onionoo: submodule-update
	$(rbm) build onionoo --target dev-build

submodule-update:
	git submodule update --init

fetch: submodule-update
	$(rbm) fetch

clean: submodule-update
	./tools/clean-old

clean-dry-run: submodule-update
	./tools/clean-old --dry-run
